#include "Board.h"
#include "Knight.h"
#include "Bishop.h"
#include "King.h"
#include "Queen.h"
#include "Pawn.h"
#include "Rook.h"


enum OUTPUTS
{
	PROPER_MOVE,
	CHECK,
	NO_TOOL_FOR_CURRENT_PLAYER,
	TAKEN_PLACE_BY_SAME_PLAYER,
	CHECK_WARNING,
	INVALID_INDEX,
	INVALID_MOVE,
	SAME_INDEX,
	CHECKMATE
};


/*
	Constructor
	Input:
		newBoard
	Output:
		None
*/
Board::Board(char* newBoard)
{
	this->_currentBoard = newBoard;
	this->createTools();
}


/*
	Function creates all the tools
	Input:
		None
	Output
		None
*/
void Board::createTools()
{
	int i = 0;
	int j = 0;
	char tool = ' ';
	string color = "";
	//declaring vars
	for (i = 0; i < BOARD_DIMENSIONS; i++)
	{
		for (j = 0; j < BOARD_DIMENSIONS; j++)
		{
			tool = this->_currentBoard[i * 8 + j];
			if (islower(tool))
			{
				color = "Black";
			}
			else
			{
				color = "White";
			}
			if (tool == 'P' || tool == 'p')
			{
				this->_board[i][j] = new Pawn(color, i * 8 + j);
			}
			else if (tool == 'N' || tool == 'n')
			{
				this->_board[i][j] = new Knight(color, i * 8 + j);
			}
			else if (tool == 'K' || tool == 'k')
			{
				this->_board[i][j] = new King(color, i * 8 + j);
			}
			else if (tool == 'Q' || tool == 'q')
			{
				this->_board[i][j] = new Queen(color, i * 8 + j);
			}
			else if (tool == 'R' || tool == 'r')
			{
				this->_board[i][j] = new Rook(color, i * 8 + j);
			}
			else if (tool == 'B' || tool == 'b')
			{
				this->_board[i][j] = new Bishop(color, i * 8 + j);
			}
			else
			{
				this->_board[i][j] = nullptr;
			}
		}
	}
}

/*
	Destructor
	Input:
		None
	Output:
		None
*/
Board::~Board()
{

}


/*
	Board setter
	Input:	
		newBoard
	Output:
		None
*/
void Board::setBoard(char* newBoard)
{
	this->_currentBoard = newBoard;
}


/*
	Function returns the current board
	Input:
		None
	Output:
		current board
*/
char* Board::getBoard()
{
	return this->_currentBoard;
}


/*
	Function updates the board
	Input:
		move 
	Output:
		None
*/
int Board::updateBoard(string move)
{
	int moveFrom = ((8 - (int(move[1]) - 48)) * 8 + int(move[0]) - 96) - 1;
	int moveTo = ((8 - (int(move[3]) - 48)) * 8 + int(move[2]) - 96) - 1;
	char* newBoard = this->_currentBoard;
	int check = 0;
	int location = 0;
	char otherTurn = '1';
	//declaring vars
	if (this->_currentBoard[64] == '0') // if it is white's turn
	{
		if ((islower(this->_currentBoard[moveFrom])) || this->_currentBoard[moveFrom] == '#')
		{
			return NO_TOOL_FOR_CURRENT_PLAYER;
		}
		else if (isupper(this->_currentBoard[moveTo]))
		{
			return TAKEN_PLACE_BY_SAME_PLAYER;
		}
	}
	else //if it is black's turn
	{
		if ((isupper(this->_currentBoard[moveFrom])) || this->_currentBoard[moveFrom] == '#')
		{
			return NO_TOOL_FOR_CURRENT_PLAYER;
		}
		else if (islower(this->_currentBoard[moveTo]))
		{
			return TAKEN_PLACE_BY_SAME_PLAYER;
		}
	}
	if (moveFrom == moveTo)
	{
		return SAME_INDEX;
	}
	if (this->_board[moveFrom / 8][moveFrom - (8 * (moveFrom / 8))] != nullptr)
	{
		check = this->_board[moveFrom / 8][moveFrom - (8 * (moveFrom / 8))]->checkMoveAvailablity(newBoard, moveTo);
	}
	if (check == PROPER_MOVE || check == CHECK)
	{	
		string currentBoard = this->_currentBoard;
		if (this->_currentBoard[64] == '0')
		{
			location = currentBoard.find("K");
		}
		else
		{
			location = currentBoard.find("k");
		}
		check = ((King*)this->_board[location / 8][location - (8 * (location / 8))])->checkChess(currentBoard, moveTo, moveFrom);
		if(otherTurn != _currentBoard[64])
		{
			if(check == CHECK)
			{
				return CHECK_WARNING;
			}	
		}
		else if(otherTurn == _currentBoard[64] && _currentBoard[64] == '1')
		{
			if (check == CHECK)
			{
				return CHECK_WARNING;
			}
		}
		this->_board[moveFrom / 8][moveFrom - (8 * (moveFrom / 8))]->setLocation(moveTo);
		newBoard[moveTo] = newBoard[moveFrom];
		newBoard[moveFrom] = '#';
		//switching chars in the newBoard
		if (this->_currentBoard[64] == '0')
		{
			newBoard[64] = '1';
			otherTurn = '0';
		}
		else
		{
			otherTurn = '1';
			newBoard[64] = '0';
		}
		this->_currentBoard = newBoard;
		if (this->_currentBoard[64] == '0')
		{
			location = currentBoard.find("K");
		}
		else
		{
			location = currentBoard.find("k");
		}
		check = ((King*)this->_board[location / 8][location - (8 * (location / 8))])->checkChess(this->_currentBoard, moveTo, moveFrom);
		this->_board[moveTo / 8][moveTo - (8 * (moveTo / 8))] = this->_board[moveFrom / 8][moveFrom - (8 * (moveFrom / 8))];
		this->_board[moveFrom / 8][moveFrom - (8 * (moveFrom / 8))] = nullptr;
		return check;
	}
	else if (check == INVALID_MOVE)
	{
		return INVALID_MOVE;
	}
	else if (check == INVALID_INDEX)
	{
		return INVALID_INDEX;
	}
	else if (check == CHECK_WARNING)
	{
		return CHECK_WARNING;
	}
	else
	{
		return CHECKMATE;
	}
}