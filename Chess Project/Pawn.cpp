#include "Pawn.h"


/*
	Constructor
	Input:
		color, location
	Output:
		None
*/
Pawn::Pawn(string color, int location) :
	Tool("Pawn", color, location)
{
	this->_firstMove = true;
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Pawn::~Pawn()
{

}


/*
	Function checks move availability
	Input:
		move from, move to
	Output:
		None
*/
int Pawn::checkMoveAvailablity(string board, int moveTo)
{
	int location = this->getLocation();
	string color = this->getColor();
	int i = 0;
	//declaring vars
	if (moveTo % 8 == location % 8) //same column
	{
		if (color == "White") //white pawn, can only go up
		{
			if ((location - moveTo == 8) || ((location - moveTo == 16) && this->_firstMove == true))
			{
				for (i = 0; i < (location - moveTo) / 8; i++)
				{
					if (board[location - (8 * (i + 1))] != '#')
					{
						return 6; //INVALID MOVEMENT, pawn moves through another tool
					}
				}
				this->_firstMove = false;
				return 0; //VALID MOVE
			}
		}
		else //black pawn, can only go down
		{
			if ((moveTo - location == 8) || ((moveTo - location == 16) && this->_firstMove == true))
			{
				for (i = 0; i < (moveTo - location) / 8; i++)
				{
					if (board[location + (8 * (i + 1))] != '#')
					{
						return 6; //INVALID MOVEMENT, pawn moves through another tool
					}
				}
				this->_firstMove = false;
				return 0;
			}
		}
		return 6; //INVALID MOVEMENT
	}
	else if (((moveTo == location + 7 || moveTo == location + 9) && color == "Black") || ((moveTo == location - 7 || moveTo == location - 9) && color == "White")) //pawn trying to eat
	{
		if (islower(board[moveTo]) && color == "White")
		{
			return 0; //white pawn eats black tool
		}
		else if (isupper(board[moveTo]) && color == "Black")
		{
			return 0; //black pawn eats white tool
		}
		else
		{
			return 6; //INVALID MOVEMENT, no tool for pawn to eat
		}
	}
	else
	{
		return 6; //INVALID MOVEMENT
	}
}