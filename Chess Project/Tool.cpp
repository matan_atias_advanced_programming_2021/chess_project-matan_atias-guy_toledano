#include "Tool.h"

/*
	Constructor
	Input:
		tool type, tool color(Black/White), tool current location
	Output:
		None
*/
Tool::Tool(string type, string color, int location)
{
	this->type = type;
	this->color = color;
	this->location = location;
}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Tool::~Tool()
{

}


/*
	Function checks the availability of a move
	Input:
		current location, location the player wants to move the piece to
	Output:
		checks if location is available
*/
int Tool::checkMoveAvailablity(string moveFrom, int moveTo)
{
	return 0;
}


/*
	getter for location
	Input:
		None
	Output:
		location*/
int Tool::getLocation()
{
	return this->location;
}


/*
	getter for color
	Input:
		None
	Output:
		color
*/
string Tool::getColor()
{
	return this->color;
}


/*
	setter for location
	Input:
		new location
	Output:
		None
*/
void Tool::setLocation(int newLocation)
{
	this->location = newLocation;
}