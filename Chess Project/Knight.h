#pragma once

#include "Tool.h"

class Knight : public Tool
{
	public:
		Knight(string color, int location); //constructor
		~Knight(); //destructor
		virtual int checkMoveAvailablity(string board, int moveTo);
};

