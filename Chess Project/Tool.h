#pragma once

#include <iostream>
#include <string>

using std::string;

class Tool
{
	public:
		Tool(string type, string color, int location); //constructor
		virtual ~Tool(); //destructor
		virtual int checkMoveAvailablity(string moveFrom, int moveTo);
		int getLocation();
		string getColor();
		void setLocation(int newLocation);
	protected:
		string type;
		string color;
		int location;
};