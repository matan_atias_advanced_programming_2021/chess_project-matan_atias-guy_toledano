#include "Bishop.h"


/*
	Constructor
	Input:
		color, location
	Output:
		None
*/
Bishop::Bishop(string color, int location) :
	Tool("Bishop", color, location)
{

}


/*
	Destructor
	Input:
		None
	Output:
		None
*/
Bishop::~Bishop()
{

}


/*
	Function checks move availability
	Input:
		move from, move to
	Output:
		None
*/
int Bishop::checkMoveAvailablity(string board, int moveTo)
{
	int location = this->getLocation();

	//declaring vars
	if (moveTo > location) //bishop wants to go down
	{
		if (location % 9 == moveTo % 9) //bishop wants to move right and down
		{
			return checkMoveFunc(1, 1, 9, board, moveTo);
		}
		else if (location % 7 == moveTo % 7) //bishop wants to move left and down
		{
			return checkMoveFunc(1, -1, 7, board, moveTo);
		}
		else
		{
			return 6; //INVALID MOVE
		}
	}
	else //bishop wants to go up
	{
		if (location % 9 == moveTo % 9) //bishop wants to move left and up
		{
			return checkMoveFunc(-1, -1, 9, board, moveTo);
			
		}
		else if (location % 7 == moveTo % 7) //bishop wants to move right and up
		{
			return checkMoveFunc(-1, 1, 7, board, moveTo);
		}
		else
		{
			return 6; //INVALID MOVE
		}
	}
}

/*
	DOWN = 1;
	UP = -1;
	LEFT = -1;
	RIGHT = 1;
*/
int Bishop::checkMoveFunc(int upOrDown, int leftOrRight, int jumps, string board, int moveTo)
{
	int location = this->getLocation();
	string color = this->getColor();
	int lineDiffer = (moveTo - location) / (jumps * upOrDown);
	int nextSquareLoc;
	
	for (int i = 0; i < lineDiffer; i++)
	{
		nextSquareLoc = location + upOrDown * jumps * (i + 1);
		if (nextSquareLoc % 8 == location % 8 + leftOrRight * (i + 1))
		{
			if (board[nextSquareLoc] != '#')
			{
				if (nextSquareLoc == moveTo)
				{
					if (color == "White" && islower(board[moveTo]))
					{
						return 0; // white bishop eats the black tool
					}
					else if (color == "Black" && isupper(board[moveTo]))
					{
						return 0; // black bishop eats the white tool
					}
				}
				return 6;
			}
		}
		else
		{
			return 6; //INVALID MOVEMENT
		}
	}
	return 0; //VALID MOVE
}